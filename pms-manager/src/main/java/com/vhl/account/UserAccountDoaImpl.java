package com.vhl.account;

import com.vhl.constant.TableType;
import com.vhl.doa.account.UserAccountDoa;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.entities.account.UserAccount;
import com.vhl.services.account.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

public class UserAccountDoaImpl implements UserAccountDoa {
    @Autowired private UserAccountRepository repo;
    @Autowired private IDGeneratorDoa gen;
    @Override
    public UserAccount save(UserAccount object) {
        object.setId(gen.generateIdOn(TableType.USER_ACCOUNT));
        repo.save(object);
        return object;
    }

    @Override
    public UserAccount update(UserAccount object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public UserAccount delete(UserAccount object) {
        repo.delete(object);
        return object;
    }

    @Override
    public UserAccount get(int id) {
        return repo.findOne(id);
    }


    @Override
    public boolean validateUser(String userName, String password) {
        try {
            return  repo.validateUser(userName, password);
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }
}
