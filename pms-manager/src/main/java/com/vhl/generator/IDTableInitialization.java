package com.vhl.generator;

import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.entities.local.CustomTableIDGenerator;


import static com.vhl.constant.TableType.*;

public class IDTableInitialization {

    IDGeneratorDoa generatorDoa;

    public void setGeneratorDoa(IDGeneratorDoa generatorDoa) {
        this.generatorDoa = generatorDoa;
    }

    public IDGeneratorDoa getGeneratorDoa() {
        return generatorDoa;
    }

    public void checkTableInitialization() {
        checkInitialization(PERSON,PERSON_NAME);
        checkInitialization(RESIDENT, RESIDENT_NAME);
        checkInitialization(REGION,REGION_NAME);
        checkInitialization(MUN_CITY, MUN_CITY_NAME);
        checkInitialization(TOWN, TOWN_NAME);
        checkInitialization(STREET, STREET_NAME);
        checkInitialization(CLIENT, CLIENT_NAME);
        checkInitialization(CONTRACT, CONTRACT_NAME);
        checkInitialization(ENGINEER, ENGINEER_NAME);
        checkInitialization(FIXES,FIXES_NAME);
        checkInitialization(MACHINE, MACHINE_NAME);
        checkInitialization(PMS,PMS_NAME);
        checkInitialization(USER_ACCOUNT, USERT_ACCOUNT_NAME);

    }

    private void checkInitialization(int id, String name) {
        System.out.println(generatorDoa);
        if (generatorDoa.get(id) == null) {
            CustomTableIDGenerator t = new CustomTableIDGenerator(id);
            t.setTable_name(name);
            generatorDoa.save(t);
        }
    }

}
