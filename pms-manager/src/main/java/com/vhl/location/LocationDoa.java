package com.vhl.location;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.location.BarangayDoa;
import com.vhl.doa.location.MunCityDoa;
import com.vhl.doa.location.RegionDoa;
import com.vhl.doa.location.StreetDoa;
import com.vhl.entities.location.Barangay;
import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.entities.location.Region;
import com.vhl.entities.location.Street;
import com.vhl.services.location.barangay.BarangayRepository;
import com.vhl.services.location.mun_city.MunCityRepository;
import com.vhl.services.location.region.RegionRepository;
import com.vhl.services.location.street.StreetRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class LocationDoa {


    //-- GETTERS > IMPLEMENTATION
    public static RegionDoa getRegionDoaImpl() {
        return new RegionDoaImpl();
    }


    public static MunCityDoa getMunCityDoaImpl() {
        return new MunCityDoaImpl();
    }

    public static BarangayDoa getBarangayDoaImpl() {
        return new BarangayDoaImpl();
    }

    public static StreetDoa getStreetDoaImpl() {
        return new StreetDoaImpl();
    }


    //-- DOA IMPLEMENTATION
    //-- REGION
    private static class RegionDoaImpl implements RegionDoa {

        @Autowired
        private RegionRepository repo;
        @Autowired private IDGeneratorDoa generator;

        @Override
        public Region save(Region object) {
            object.setId(generator.generateIdOn(TableType.REGION));
            repo.save(object);
            return object;
        }

        @Override
        public Region update(Region object){
            repo.save(object);
            return object;
        }

        @Override
        public int delete(int id){
            repo.delete(id);
            return id;
        }

        @Override
        public Region delete(Region object){
            repo.delete(object);
            return object;
        }

        @Override
        public Region get(int id){
            return repo.findOne(id);
        }

        @Override
        public List<Region> findRegionLike(String queryString) {
            return repo.findRegionLike(queryString);
        }

        @Override
        public Region getExistingRegionLike(String queryString) {
            return repo.getExistingRegionLike(queryString);
        }

        @Override
        public List<Municipality_CityTown> findMunicipality_TownCityByRegion(int regionID) {
            return repo.findMunicipality_TownCityByRegion(regionID);
        }
    }


    //-- MUNICIPALITY AND CITY
    private static class MunCityDoaImpl implements MunCityDoa {

        @Autowired private MunCityRepository repo;
        @Autowired private IDGeneratorDoa generator;

        @Override
        public Municipality_CityTown save(Municipality_CityTown object){
            object.setId(generator.generateIdOn(TableType.MUN_CITY));
            repo.save(object);
            return object;
        }

        @Override
        public Municipality_CityTown update(Municipality_CityTown object) {
            repo.save(object);
            return object;
        }

        @Override
        public int delete(int id){
            repo.delete(id);
            return id;
        }

        @Override
        public Municipality_CityTown delete(Municipality_CityTown object) {
            repo.delete(object);
            return object;
        }

        @Override
        public Municipality_CityTown get(int id) {
            return repo.findOne(id);
        }

        @Override
        public List<Municipality_CityTown> findMunicipalityCityLike(String queryString) {
            return repo.findMunicipalityCityLike(queryString);
        }

        @Override
        public List<Municipality_CityTown> findMunicipalityCityLikeOn(Region region, String queryString) {
            return repo.findMunicipalityCityLikeOn(region, queryString);
        }
    }



    //-- BARANGAY/TOWN
    public static class BarangayDoaImpl implements BarangayDoa {

        @Autowired private BarangayRepository repo;
        @Autowired private IDGeneratorDoa generator;

        @Override
        public Barangay save(Barangay object){
            object.setId(generator.generateIdOn(TableType.TOWN));
            repo.save(object);
            return object;
        }

        @Override
        public Barangay update(Barangay object) {
            repo.save(object);
            return object;
        }

        @Override
        public int delete(int id) {
            repo.delete(id);
            return id;
        }

        @Override
        public Barangay delete(Barangay object) {
            repo.delete(object);
            return object;
        }

        @Override
        public Barangay get(int id) {
            return repo.findOne(id);
        }

        @Override
        public List<Barangay> findBarangayLike(String queryString) {
            return repo.findBarangayLike(queryString);
        }

        @Override
        public List<Barangay> findBarangayLikeOn(Municipality_CityTown muncity, String queryString) {
            return repo.findBarangayLikeOn(muncity, queryString);
        }
    }


    //-- STREET
    private static class StreetDoaImpl implements StreetDoa {

        @Autowired private StreetRepository repo;
        @Autowired private IDGeneratorDoa generator;

        @Override
        public Street save(Street object) {
            object.setId(generator.generateIdOn(TableType.STREET));
            repo.save(object);
            return object;
        }

        @Override
        public Street update(Street object){
            repo.save(object);
            return object;
        }

        @Override
        public int delete(int id)  {
            repo.delete(id);
            return id;
        }

        @Override
        public Street delete(Street object)  {
            repo.delete(object);
            return object;
        }

        @Override
        public Street get(int id)  {
            return repo.findOne(id);
        }

        @Override
        public List<Street> findStreetLike(String queryString) {
            return repo.findStreetLike(queryString);
        }

        @Override
        public List<Street> findStreetLikeOn(Barangay barangay, String locationName) {
            return repo.findStreetLikeOn(barangay, locationName);
        }
    }
}
