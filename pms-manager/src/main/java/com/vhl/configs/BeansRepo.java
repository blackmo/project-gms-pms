package com.vhl.configs;

import com.vhl.constant.ServiceName;
import com.vhl.doa.account.UserAccountDoa;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.local.PersonDoa;
import com.vhl.doa.local.ResidentDoa;
import com.vhl.doa.location.BarangayDoa;
import com.vhl.doa.location.MunCityDoa;
import com.vhl.doa.location.RegionDoa;
import com.vhl.doa.location.StreetDoa;
import com.vhl.doa.priv.*;
import com.vhl.account.UserAccountDoaImpl;
import com.vhl.local.CustomIdGenerator;
import com.vhl.local.PersonDoaImpl;
import com.vhl.local.ResidentDoaImpl;
import com.vhl.location.LocationDoa;
import com.vhl.prv.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
@EnableJpaRepositories("${services}")
@EntityScan("${entities}")
public class BeansRepo {


    @Bean
    IDGeneratorDoa getGeneratorDoa() {
        System.out.println("hello");
        return CustomIdGenerator.getDoaImplementation();
    }

    //-- LOCAL
    @Bean
    public PersonDoa getPersonDoa() {
        return new PersonDoaImpl();
    }

    @Bean
    public ResidentDoa getResidentDoa() {
        return new ResidentDoaImpl();
    }

    //-- LOCATION
    @Bean
    public RegionDoa getRegionDoa() {
        return LocationDoa.getRegionDoaImpl();
    }

    @Bean
    public MunCityDoa getMunCityDoa() {
        return LocationDoa.getMunCityDoaImpl();
    }

    @Bean
    public BarangayDoa getBarangayDoa() {
        return LocationDoa.getBarangayDoaImpl();
    }

    @Bean
    public StreetDoa getStreetDoa() {
        return LocationDoa.getStreetDoaImpl();
    }

    //-- PRIV
    @Bean
    ClientDoa getClientDoa() {
        return new ClientDoaImpl();
    }

    @Bean
    ContractDoa getContractDoa() {
        return new ContractImpl();
    }

    @Bean
    EmployeeDoa getEmployeeDoa() {
        return new EmployeeDoaImpl();
    }

    @Bean
    FixesDoa getFixesDoa() {
        return new FixesDoaImpl();
    }


    @Bean
    MachineDoa getMachineDoa() {
        return new MachineDoaImpl();
    }

    @Bean
    PMSDoa getPmsDoa() {
        return new PMSDoaImpl();
    }

    @Bean
    UserAccountDoa getUserAccountDoa() {
        return new UserAccountDoaImpl();
    }


    //-- HTTP INVOKERS
    @Bean(name = ServiceName.PERON_SERVICE)
    HttpInvokerServiceExporter pesonService(PersonDoa personDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(personDoa);
        exp.setServiceInterface(PersonDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.RESIDENT_SERVICE)
    HttpInvokerServiceExporter residentService(ResidentDoa residentDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(residentDoa);
        exp.setServiceInterface(ResidentDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.REGION_SERVICE)
    HttpInvokerServiceExporter regionServices(RegionDoa regionDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(regionDoa);
        exp.setServiceInterface(RegionDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.MUNCITY_SERVICE)
    HttpInvokerServiceExporter muncityService(MunCityDoa munCityDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(munCityDoa);
        exp.setServiceInterface(MunCityDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.BARANGAY_SERVICE)
    HttpInvokerServiceExporter barangayService(BarangayDoa barangayDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(barangayDoa);
        exp.setServiceInterface(BarangayDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.STREET_SERVICE)
    HttpInvokerServiceExporter streetService(StreetDoa streetDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(streetDoa);
        exp.setServiceInterface(StreetDoa.class);
        return exp;

    }

    @Bean(name = ServiceName.CLIENT_SERVICE)
    HttpInvokerServiceExporter clientService(ClientDoa clientDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(clientDoa);
        exp.setServiceInterface(ClientDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.CONTRACT_SERVICE)
    HttpInvokerServiceExporter contractService(ContractDoa contractDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(contractDoa);
        exp.setServiceInterface(ContractDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.EMPLOYEE_SERVICE)
    HttpInvokerServiceExporter employeeService(EmployeeDoa employeeDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(employeeDoa);
        exp.setServiceInterface(EmployeeDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.FIXES_SERVICE)
    HttpInvokerServiceExporter fixeService(FixesDoa fixesDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(fixesDoa);
        exp.setServiceInterface(FixesDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.MACHINE_SERVICE)
    HttpInvokerServiceExporter machineServices(MachineDoa machineDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(machineDoa);
        exp.setServiceInterface(MachineDoa.class);
        return exp;

    }

    @Bean(name = ServiceName.PMS_SERVICE)
    HttpInvokerServiceExporter pmsService(PMSDoa pmsDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(pmsDoa);
        exp.setServiceInterface(PMSDoa.class);
        return exp;
    }

    @Bean(name = ServiceName.USER_ACCOUNT_SERVICE)
    HttpInvokerServiceExporter userAccountService(UserAccountDoa userAccountDoa) {
        HttpInvokerServiceExporter exp = new HttpInvokerServiceExporter();
        exp.setService(userAccountDoa);
        exp.setServiceInterface(UserAccountDoa.class);
        return exp;
    }
}
