package com.vhl.prv;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.priv.ContractDoa;
import com.vhl.entities.priv.Contract;
import com.vhl.services.priv.contract.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class ContractImpl implements ContractDoa {

    @Autowired private ContractRepository repo;
    @Autowired private IDGeneratorDoa gen;

    @Override
    public Contract save(Contract object) {
        object.setId(gen.generateIdOn(TableType.CONTRACT));
        repo.save(object);
        return object;
    }

    @Override
    public Contract update(Contract object) {
        repo.save(object);
        return null;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public Contract delete(Contract object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Contract get(int id) {
        return repo.findOne(id);
    }
}
