package com.vhl.prv;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.priv.FixesDoa;
import com.vhl.entities.priv.Fixes;
import com.vhl.services.priv.fixes.FixesRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class FixesDoaImpl implements FixesDoa {

    @Autowired private FixesRepository repo;
    @Autowired private IDGeneratorDoa gen;

    @Override
    public Fixes save(Fixes object) {
        object.setId(gen.generateIdOn(TableType.FIXES));
        repo.save(object);
        return object;
    }

    @Override
    public Fixes update(Fixes object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public Fixes delete(Fixes object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Fixes get(int id) {
        return repo.findOne(id);
    }
}
