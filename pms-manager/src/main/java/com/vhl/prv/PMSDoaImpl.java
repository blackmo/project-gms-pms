package com.vhl.prv;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.priv.PMSDoa;
import com.vhl.entities.priv.PMS;
import com.vhl.services.priv.pms.PMSRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PMSDoaImpl implements PMSDoa {

    @Autowired private PMSRepository repo;
    @Autowired private IDGeneratorDoa gen;

    @Override
    public PMS save(PMS object) {
        object.setId(gen.generateIdOn(TableType.PMS));
        return null;
    }

    @Override
    public PMS update(PMS object) {
        return null;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public PMS delete(PMS object) {
        return null;
    }

    @Override
    public PMS get(int id) {
        return null;
    }
}
