package com.vhl.prv;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.priv.EmployeeDoa;
import com.vhl.entities.priv.Employee;
import com.vhl.services.priv.employee.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeDoaImpl implements EmployeeDoa {


    @Autowired private EmployeeRepository repo;
    @Autowired private IDGeneratorDoa gen;

    @Override
    public Employee save(Employee object) {
        object.setId(gen.generateIdOn(TableType.ENGINEER));
        repo.save(object);
        return object;
    }

    @Override
    public Employee update(Employee object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public Employee delete(Employee object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Employee get(int id) {
        return null;
    }
}
