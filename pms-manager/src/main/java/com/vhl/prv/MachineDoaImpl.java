package com.vhl.prv;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.priv.MachineDoa;
import com.vhl.entities.priv.Machine;
import com.vhl.services.priv.machine.MachineRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class MachineDoaImpl implements MachineDoa {

    @Autowired private MachineRepository repo;
    @Autowired private IDGeneratorDoa gen;

    @Override
    public Machine save(Machine object) {
        object.setId(gen.generateIdOn(TableType.MACHINE));
        repo.save(object);
        return object;
    }

    @Override
    public Machine update(Machine object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public Machine delete(Machine object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Machine get(int id) {
        return repo.findOne(id);
    }
}
