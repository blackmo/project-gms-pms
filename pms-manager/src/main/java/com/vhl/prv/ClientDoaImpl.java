package com.vhl.prv;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.priv.ClientDoa;
import com.vhl.entities.priv.Client;
import com.vhl.services.priv.client.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class ClientDoaImpl implements ClientDoa{

    @Autowired private ClientRepository repo;
    @Autowired private IDGeneratorDoa gen;


    @Override
    public Client save(Client object) {
        object.setId(gen.generateIdOn(TableType.CLIENT));
        repo.save(object);
        return object;
    }

    @Override
    public Client update(Client object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public Client delete(Client object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Client get(int id) {
        return repo.findOne(id);
    }

    @Override
    public List<Client> getAllClient() {
        ArrayList<Client> list = new ArrayList<>();
        repo.findAll().forEach(list::add);
        return list;
    }
}
