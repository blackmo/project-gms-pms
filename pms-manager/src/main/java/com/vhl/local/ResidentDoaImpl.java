package com.vhl.local;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.local.ResidentDoa;
import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;
import com.vhl.services.local.residence.ResidenceRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ResidentDoaImpl implements ResidentDoa {

    @Autowired private ResidenceRepository repo;
    @Autowired private IDGeneratorDoa generator;


    @Override
    public Resident save(Resident object){
        object.setId(generator.generateIdOn(TableType.RESIDENT));
        repo.save(object);
        return object;
    }

    @Override
    public Resident update(Resident object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id) {
        repo.delete(id);
        return id;
    }

    @Override
    public Resident delete(Resident object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Resident get(int id) {
        return repo.findOne(id);
    }

    @Override
    public List<Resident> getResidentByPersonId(int id) {
        return repo.getResidentByPersonId(id);
    }

    @Override
    public List<Resident> getResidentByArea(Place place) {
        return repo.getResidentByArea(place);
    }
}
