package com.vhl.local;

import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.entities.local.CustomTableIDGenerator;
import com.vhl.services.local.custom_tableGen.CustomTableIDGenRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class CustomIdGenerator {


    public static IDGeneratorDoa getDoaImplementation() {
        return new DoaImplementation();
    }

    private static class DoaImplementation implements IDGeneratorDoa {

        private CustomTableIDGenerator generator;

        @Autowired
        private CustomTableIDGenRepository repo;

        @Override
        public CustomTableIDGenerator save(CustomTableIDGenerator object){
            repo.save(object);
            return object;
        }

        @Override
        public CustomTableIDGenerator update(CustomTableIDGenerator object){
            repo.save(object);
            return object;
        }

        @Override
        public int delete(int id) {
            repo.delete(id);
            return id;
        }

        @Override
        public CustomTableIDGenerator delete(CustomTableIDGenerator object){
            repo.delete(object);
            return object;
        }

        @Override
        public CustomTableIDGenerator get(int id)  {
            return repo.findOne(id);
        }

        @Override
        public int generateIdOn(int tableType) {
            int id;
            generator = repo.findOne(tableType);
            id = generator.generateID();
            repo.save(generator);
            return id;
        }
    }

}
