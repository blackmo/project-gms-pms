package com.vhl.local;

import com.vhl.constant.TableType;
import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.doa.local.PersonDoa;
import com.vhl.entities.local.Person;
import com.vhl.services.local.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PersonDoaImpl implements PersonDoa {

    @Autowired private PersonRepository repo;
    @Autowired private IDGeneratorDoa generator;

    @Override
    public Person save(Person object) {
        object.setId(generator.generateIdOn(TableType.PERSON));
        repo.save(object);
        return object;
    }

    @Override
    public Person update(Person object) {
        repo.save(object);
        return object;
    }

    @Override
    public int delete(int id){
        repo.delete(id);
        return id;
    }

    @Override
    public Person delete(Person object) {
        repo.delete(object);
        return object;
    }

    @Override
    public Person get(int id) {
        return repo.findOne(id);
    }

    @Override
    public void createContact(int personID, int contactsID) {
        repo.createContact(personID, contactsID);
    }
}
