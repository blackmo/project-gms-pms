package com.vhl.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.vhl")
public class ApplicationConfiguration {
    @Bean
    Sample getSample() {
        System.out.println("sample bean created");
        return  new Sample();

    }
}
