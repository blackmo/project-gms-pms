package com.vhl.register;

import com.jfoenix.controls.*;
import com.vhl.common.utitities.CommonUtils;
import com.vhl.common.utitities.com.vhl.commmon.fx.FXutils;
import com.vhl.constant.Type;
import com.vhl.constants.Constants;
import com.vhl.dialog_creator.MaterialDialog;
import com.vhl.doa.account.UserAccountDoa;
import com.vhl.doa.location.RegionDoa;
import com.vhl.entities.local.Person;
import com.vhl.entities.location.Region;
import de.felixroske.jfxsupport.FXMLController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.time.ZoneId;

import static com.vhl.constants.Constants.*;

@FXMLController
public class RegisterController {


    @Autowired private UserAccountDoa userAccountDoa;
    @Autowired private RegionDoa regionDoa;

    @FXML
    private StackPane root;

    @FXML
    private JFXTextField firstName;

    @FXML
    private JFXTextField middleName;

    @FXML
    private JFXTextField lastName;

    @FXML
    private JFXTextField contactNumber;

    @FXML
    private JFXTextField street;

    @FXML
    private JFXTextField village;

    @FXML
    private JFXTextField muncity;

    @FXML
    private JFXTextField region;

    @FXML
    private JFXComboBox<String> comboPosition;

    @FXML
    private JFXTextField username;

    @FXML
    private JFXPasswordField password;

    @FXML
    private JFXPasswordField conPassword;

    @FXML
    private JFXButton buttonRegister;

    @FXML
    private JFXButton buttonCancel;

    @FXML
    private JFXDatePicker birthdate;

    @FXML
    private JFXRadioButton rbMale, rbFemale;

    private TextArea ta;
    private boolean emptyFields;
    private boolean dateEmpty;
    private boolean passwordNotEqual;
    private MaterialDialog dialogConfirmation, dialogError;
    private int gender;


    public void initialize() {
        initComboBox();
        initTableView();
        initToggleGroup();
        initTextFields();
    }

    //-- PRIVATE METHODS
    private void createDialogConfirmation() {
        if (dialogConfirmation == null)
            dialogConfirmation = new MaterialDialog(root, "ConfirmRegistration");
        JFXButton confirm = new JFXButton("Confirm");
        confirm.setPrefWidth(100);
        confirm.getStylesheets().add(this.getClass().getResource("/com/vhl/stylesheet/buttons/button-style.css").toExternalForm());
        confirm.getStyleClass().add("gms-theme-button");
        confirm.setOnAction(event1 -> {
            dialogConfirmation.getDialog().close();
        });

        JFXButton back = new JFXButton("Back");
        back.setPrefWidth(100);
        back.getStylesheets().add(this.getClass().getResource("/com/vhl/stylesheet/buttons/button-style.css").toExternalForm());
        back.getStyleClass().add("gms-theme-button");
        back.setOnAction(event1 -> {
            ta.setText("");
            dialogConfirmation.getDialog().close();
        });

        HBox box = new HBox(back, confirm);
        box.setSpacing(10);

        dialogConfirmation.setActions(box);
    }

    private void createDialogError() {
        dialogError = new MaterialDialog(root, "Empty Values");
        JFXButton ok = new JFXButton("OK");
        ok.setPrefWidth(100);
        ok.getStylesheets().add(this.getClass().getResource("/com/vhl/stylesheet/buttons/button-style.css").toExternalForm());
        ok.getStyleClass().add("gms-theme-button");
        ok.setOnAction(event -> {
            ta.setText("");
            dialogError.getDialog().close();
        });

        dialogError.setActions(ok);
    }


    private void initComboBox() {
        comboPosition.getItems().addAll(Constants.POSITIONS);
        comboPosition.getSelectionModel().select(0);
    }

    private void initTextFields() {
        FXutils.alphaNumericFields(password, 16);
        FXutils.alphaNumericFields(username, 12);
        FXutils.allNumericField(contactNumber, 11);
    }

    private void initToggleGroup() {
        ToggleGroup group = new ToggleGroup();
        rbMale.setSelected(true);
        rbMale.setToggleGroup(group);
        rbFemale.setToggleGroup(group);

        group.selectedToggleProperty().addListener(observable -> {
            if (rbMale.isSelected()) gender = Type.MALE;
            else gender = Type.FEMALE;

        });
    }

    private void initTableView() {
        ta = new TextArea();
        ta.setEditable(false);
        ta.getStylesheets().add(this.getClass().getResource("/com/vhl/stylesheet/removed-focuse.css").toExternalForm());
    }

    private void checkForEmptyFields() {
        emptyFields =
                firstName.textProperty().isEmpty().get() ||
                        lastName.textProperty().isEmpty().get() ||
                        muncity.textProperty().isEmpty().get() ||
                        region.textProperty().isEmpty().get() ||
                        username.textProperty().isEmpty().get() ||
                        password.textProperty().isEmpty().get() ||
                        conPassword.textProperty().isEmpty().get();
    }

    private void checkEqualPassword() {
        passwordNotEqual = !password.getText().trim().equals(conPassword.getText().trim());
    }

    private void checkDateValue() {
        dateEmpty = birthdate.getValue() == null;
    }

    private void putAlertValues() {
        if (emptyFields) ta.appendText("fill required field values indicated with asterisk *" + NL);
        if (dateEmpty) ta.appendText("specify birthday" + NL);
        if (passwordNotEqual) ta.appendText("password mismatch");
    }

    private void getTextFieldValues() {
        ta.appendText(lastName.getText().trim());
        ta.appendText(COMMA + firstName.getText().trim() + SPACE + middleName.getText().trim());
        ta.appendText(NL);
        ta.appendText("Contact Number: " + contactNumber.getText().trim());
        ta.appendText(NL);
        ta.appendText("Birthday(mm/dd/yyyy): " + CommonUtils.getSimpleDateFormat(Date.from(
                birthdate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()
        )));
        ta.appendText(NL + NL);
        ta.appendText("Address:" + NL);
        if (!street.textProperty().isEmpty().get()) ta.appendText(street.getText().trim());
        if (!village.textProperty().isEmpty().get()) ta.appendText(COMMA + village.getText().trim());
        if (!muncity.textProperty().isEmpty().get()) ta.appendText(COMMA + muncity.getText().trim());
        if (!region.textProperty().isEmpty().get()) ta.appendText(COMMA + region.getText().trim());

        ta.appendText(NL + NL);
        if (!username.textProperty().isEmpty().get()) ta.appendText("User Name: " + username.getText().trim());
    }

    private void atConfirmRegistration() {
        Person person = new Person();
        person.setFirstName(firstName.getText());
        person.setMiddleName(middleName.getText());
        person.setLastName(lastName.getText());
        person.setContactNo(contactNumber.getText());
        person.setGender((short) gender);
        person.setBirthday(Date.from(
                birthdate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));

        Region region = new Region();
        region.setLocationName(this.region.getText().trim());


    }


    //-- PUBLIC METHODS
    @FXML
    public void registerAction(ActionEvent event) {
        if (dialogError == null) createDialogError();
        if (dialogConfirmation == null) createDialogConfirmation();
        checkDateValue();
        checkForEmptyFields();
        checkEqualPassword();
        if (emptyFields || dateEmpty || passwordNotEqual) {
            putAlertValues();
            dialogError.setBody(ta);
            dialogError.displayDialog();
        } else {
            getTextFieldValues();
            dialogConfirmation.setBody(ta);
            dialogConfirmation.displayDialog();
        }
    }


    @FXML
    public void cancelRegistration(ActionEvent event) {
        Stage stage = (Stage) buttonCancel.getScene().getWindow();
        stage.close();
    }
}
