package com.vhl.dialog_creator;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class MaterialDialog extends JFXDialogLayout{
    Text header = new Text();
    Text messageBody = new Text();
    JFXDialog dialog = new JFXDialog();
    StackPane root;

    public static MaterialDialog instance;


    //-- PUBLIC CONSTRUCTORS
    public MaterialDialog(StackPane root, String header, String message) {
        this.root = root;
        this.header.setText(header);
        messageBody.setText(message);
        setHeading(this.header);
        setBody(messageBody);
    }

    public MaterialDialog(StackPane root, String header) {
        this.root = root;
        this.header.setText(header);
        setHeading(this.header);
    }

    public static MaterialDialog getInstance() {
        return instance;
    }


    //-- STATIC METHODS
    public static MaterialDialog getInstance(StackPane root, String header, String message) {
        if (instance==null) instance = new MaterialDialog(root, header, message);
        else {
            instance.root = root;
            instance.header.setText(header);
            instance.messageBody.setText(message);
        }
        return instance;
    }

    public static MaterialDialog getInstance(StackPane root, String header) {
        if (instance==null) instance = new MaterialDialog(root, header);
        else {
            instance.root = root;
            instance.header.setText(header);
        }
        return instance;
    }


    //-- PUBLIC METHODS

    public void setHeader(String header) {
        this.header.setText(header);
    }

    public void setMessageBody(String messageBody) {
        this.messageBody.setText(messageBody);
    }

    public JFXDialog getDialog() {
        return dialog;
    }

    public void setBodyContent(Node node) {
        setBody(node);
    }

    public void displayDialog() {
        dialog.setDialogContainer(root);
        dialog.setContent(this);
        dialog.setTransitionType(JFXDialog.DialogTransition.CENTER);
        dialog.show();
    }

}
