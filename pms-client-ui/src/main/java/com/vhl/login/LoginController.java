package com.vhl.login;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.vhl.dialog_creator.MaterialDialog;
import com.vhl.doa.account.UserAccountDoa;
import com.vhl.register.RegisterView;
import de.felixroske.jfxsupport.FXMLController;
import de.felixroske.jfxsupport.FXMLView;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;


/**
 * Created by blackmo on 3/7/17.
 */

@FXMLController
public class LoginController {

    @Autowired
    private RegisterView registerView;
    @Autowired
    private UserAccountDoa userAccountDoa;


    @FXML
    private StackPane root;

    @FXML
    private JFXTextField user_field;

    @FXML
    private JFXPasswordField password_field;

    @FXML
    private Button button_login;

    private Scene scene;


    public void initialize() {
        scene = new Scene(registerView.getView());

        password_field.disableProperty().bind(user_field.textProperty().isEmpty());
        button_login.disableProperty().bind(
                user_field.textProperty().isEmpty()
                        .or(password_field.textProperty().isEmpty())
        );
    }

    @FXML
    public void validateUserLogin() {
        try {
            boolean login = userAccountDoa.validateUser(user_field.getText().trim(), password_field.getText().trim());
            System.out.println("attempting login: " + login);
        } catch (RemoteConnectFailureException ex) {

            MaterialDialog materialDialog = new MaterialDialog(root, "error", "cant connect");
            JFXButton button = new JFXButton("OK");
            button.getStylesheets().add(this.getClass().getResource("/com/vhl/stylesheet/buttons/button-style.css").toExternalForm());
            button.setPrefWidth(100);
            button.getStyleClass().add("gms-theme-button");

            button.setOnAction(event -> {
                materialDialog.getDialog().close();
            });

            materialDialog.setActions(button);
            materialDialog.displayDialog();

        }
    }

    @FXML
    public void aboutUsAction(MouseEvent event) {

    }

    @FXML
    public void registerAction(MouseEvent event) {
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.showAndWait();
    }

    @FXML
    public void cancelRegistration(MouseEvent event) {

    }
}
