package com.vhl.constants;

public interface Constants {
    String[] POSITIONS = {"engineer", "app-specialist", "supervisor", "CSA"
    ,"lab-specialist"};

    String SPACE = " ";
    String COMMA = ", ";
    String NL = "\n";
}
