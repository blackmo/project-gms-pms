package com.vhl.services.local.person;

import com.vhl.entities.local.Person;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

public class PersonRepositoryImpl implements PersonCustomRepository {

    @Autowired
    private EntityManager em;
    @Autowired
    // FIXME: 10/11/17 this implementation is wrong, no original repository should circle to its implementation
    PersonRepository pr;

    @Override
    public void createContact(int personID, int contactsID){
        Person p,c;
        p=c=null;
        p = em.find(Person.class, personID);
        c = em.find(Person.class, contactsID);
        p.addContactPerson(c);
        pr.save(p);
    }
}
