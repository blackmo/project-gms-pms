package com.vhl.services.local.custom_tableGen;

import com.vhl.entities.local.CustomTableIDGenerator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CustomTableIDGenRepository extends CrudRepository<CustomTableIDGenerator, Integer>{
}
