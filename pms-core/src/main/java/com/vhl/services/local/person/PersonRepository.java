package com.vhl.services.local.person;

import com.vhl.entities.local.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface PersonRepository extends CrudRepository<Person, Integer>, PersonCustomRepository {
}
