package com.vhl.services.local.residence;

import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;

import java.util.List;

public interface ResidenceCustomService {
    List<Resident> getResidentByPersonId(int id);

    List<Resident> getResidentByArea(Place place);
}
