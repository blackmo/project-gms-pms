package com.vhl.services.local.residence;

import com.vhl.constant.TableType;
import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ResidenceRepositoryImpl implements ResidenceCustomService{

    @Autowired
    private EntityManager em;

    @Override
    public List<Resident> getResidentByPersonId(int id) {
        List<Resident> result;
        Query query;
        query = em.createQuery("select p.listResident from Person p where p.id = :id")
                .setParameter("id", id);
        return query.getResultList();

    }

    @Override
    public List<Resident> getResidentByArea(Place place) {
        Query query = null;
        List<Resident> result;
        switch (place.typeOf()) {
            case TableType.STREET:
                query = em.createQuery("from Resident r where r.street=:place");
                break;
            case TableType.TOWN:
                query = em.createQuery("from Resident r where r.barangay=:place");
                break;
            case TableType.MUN_CITY:
                query = em.createQuery("from Resident r where r.municipality_city=:place");
                break;
            case TableType.REGION:
                query = em.createQuery("from Resident r where r.region=:place");
                break;
        }
        assert query != null;
        query.setParameter("place", place);
        result = query.getResultList();
        return result;
    }
}
