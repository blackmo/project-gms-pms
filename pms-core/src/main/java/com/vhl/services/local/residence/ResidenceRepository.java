package com.vhl.services.local.residence;

import com.vhl.entities.local.Resident;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ResidenceRepository extends CrudRepository<Resident,Integer>, ResidenceCustomService {
}
