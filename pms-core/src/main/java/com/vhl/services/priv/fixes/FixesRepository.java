package com.vhl.services.priv.fixes;

import com.vhl.entities.priv.Fixes;
import org.springframework.data.repository.CrudRepository;

public interface FixesRepository extends CrudRepository<Fixes, Integer>, FixesCustomRepository {
}
