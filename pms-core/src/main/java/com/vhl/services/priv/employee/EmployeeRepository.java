package com.vhl.services.priv.employee;

import com.vhl.entities.priv.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Integer>, EmployeeCustomRepository {
}
