package com.vhl.services.priv.pms;

import com.vhl.entities.priv.PMS;
import org.springframework.data.repository.CrudRepository;

public interface PMSRepository extends CrudRepository<PMS, Integer>, PMSCustomRepository {
}
