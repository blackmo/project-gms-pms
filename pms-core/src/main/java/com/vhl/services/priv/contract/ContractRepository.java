package com.vhl.services.priv.contract;

import com.vhl.entities.priv.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Integer>, ContractCustomRepository {
}
