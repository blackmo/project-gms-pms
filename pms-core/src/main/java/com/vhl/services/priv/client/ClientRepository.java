package com.vhl.services.priv.client;

import com.vhl.entities.priv.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Integer>, ClientCustomRepository {
}
