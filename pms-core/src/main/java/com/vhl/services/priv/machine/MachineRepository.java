package com.vhl.services.priv.machine;

import com.vhl.entities.priv.Machine;
import org.springframework.data.repository.CrudRepository;

public interface MachineRepository extends CrudRepository<Machine, Integer>, MachineCustomRepository{
}
