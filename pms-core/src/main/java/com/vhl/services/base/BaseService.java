package com.vhl.services.base;

import java.rmi.RemoteException;

/**
 * Created by Knight-en on 03/08/2017.
 */
public interface BaseService<S>{
    S save(S object);

    S update(S object);

    int delete(int id);

    S delete(S object);

    S get(int id);
}
