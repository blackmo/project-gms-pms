package com.vhl.services.location.barangay;

import com.vhl.entities.location.Barangay;
import com.vhl.entities.location.Municipality_CityTown;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class BarangayRepositoryImpl implements BarangayCustomRepository {

    @Autowired
    private EntityManager em;

    @Override
    public List<Barangay> findBarangayLike(String queryString) {
        List result;
        result = em.createQuery("from Barangay b where b.locationName like :args")
                .setParameter("args", "%"+queryString+"%")
                .getResultList();
        return result;
    }

    @Override
    public List<Barangay> findBarangayLikeOn(Municipality_CityTown muncity, String queryString) {
        List result;
        result = em.createQuery("from Barangay b where b.muncity = :muncity and " +
                "b.locationName=:queryString")
                .setParameter("muncity", muncity)
                .setParameter("locationName", "%"+queryString+"%")
                .getResultList();
        return result;
    }
}
