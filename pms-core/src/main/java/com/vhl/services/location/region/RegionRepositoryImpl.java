package com.vhl.services.location.region;

import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.entities.location.Region;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class RegionRepositoryImpl implements RegionCustomRepository {

    @Autowired
    EntityManager em;

    @Override
    public List<Region> findRegionLike(String queryString) {
        List<Region> result = null;
        result = em.createQuery("from Region r where r.locationName like :queryString")
                .setParameter("queryString", queryString)
                .getResultList();
        return result;
    }

    @Override
    public Region getExistingRegionLike(String queryString) {
        Region result = null;
        result = (Region) em.createQuery("from Region r where r.locationName like :queryString")
                .setParameter("queryString", queryString)
                .getSingleResult();
        return result;
    }

    @Override
    public List<Municipality_CityTown> findMunicipality_TownCityByRegion(int regionID) {
        List<Municipality_CityTown> result = null;
        Region region = (Region) em.createQuery("from Region r where r.id=:regionID")
                .setParameter("regionID", regionID)
                .getSingleResult();
        result = em.createQuery("from municipality_city m where m.region=:region")
                .setParameter("region", region)
                .getResultList();
        return result;
    }
}
