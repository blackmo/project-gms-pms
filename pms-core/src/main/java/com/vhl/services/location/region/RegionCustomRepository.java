package com.vhl.services.location.region;


import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.entities.location.Region;

import java.util.List;

public interface RegionCustomRepository {

    List<Region> findRegionLike(String queryString);

    Region getExistingRegionLike(String queryString);

    List<Municipality_CityTown> findMunicipality_TownCityByRegion(int regionID);
}
