package com.vhl.services.location.street;

import com.vhl.entities.location.Street;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface StreetRepository extends CrudRepository<Street, Integer>, StreetCustomRepository{
}
