package com.vhl.services.location.mun_city;

import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.entities.location.Region;

import java.util.List;

public interface MunCityCustomRepository {
    List<Municipality_CityTown> findMunicipalityCityLike(String queryString);

    List<Municipality_CityTown> findMunicipalityCityLikeOn(Region region, String queryString);
}
