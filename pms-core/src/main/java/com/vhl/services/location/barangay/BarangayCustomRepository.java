package com.vhl.services.location.barangay;

import com.vhl.entities.location.Barangay;
import com.vhl.entities.location.Municipality_CityTown;

import java.util.List;

public interface BarangayCustomRepository {
    List<Barangay> findBarangayLike(String queryString);

    List<Barangay> findBarangayLikeOn(Municipality_CityTown muncity, String queryString);
}
