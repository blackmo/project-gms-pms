package com.vhl.services.location.mun_city;

import com.vhl.entities.location.Municipality_CityTown;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface MunCityRepository extends CrudRepository<Municipality_CityTown, Integer>, MunCityCustomRepository {
}
