package com.vhl.services.location.barangay;

import com.vhl.entities.location.Barangay;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface BarangayRepository extends CrudRepository<Barangay, Integer>, BarangayCustomRepository {
}
