package com.vhl.services.location.region;

import com.vhl.entities.location.Region;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface RegionRepository extends CrudRepository<Region, Integer>, RegionCustomRepository{
}
