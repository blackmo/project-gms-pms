package com.vhl.services.location.street;

import com.vhl.entities.location.Barangay;
import com.vhl.entities.location.Street;

import java.util.List;

public interface StreetCustomRepository {
    List<Street> findStreetLike(String queryString);

    List<Street> findStreetLikeOn(Barangay barangay, String locationName);
}
