package com.vhl.services.location.mun_city;

import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.entities.location.Region;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class MunCityRepositoryImpl implements MunCityCustomRepository {

    @Autowired
    private EntityManager em;

    @Override
    public List<Municipality_CityTown> findMunicipalityCityLike(String queryString) {
        List result = null;
        result = em.createQuery("from municipality_city m where m.locationName like :arg")
                .setParameter("arg", queryString)
                .getResultList();
        return result;
    }

    @Override
    public List<Municipality_CityTown> findMunicipalityCityLikeOn(Region region, String queryString) {
        List result = null;
        result = em.createQuery("from municipality_city m where m.region=:region and "+
        "m.locationName like :queryString")
                .setParameter("region", region)
                .setParameter("queryString", queryString)
                .getResultList();
        return result;
    }
}
