package com.vhl.services.location.street;

import com.vhl.entities.location.Barangay;
import com.vhl.entities.location.Street;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class StreetRepositoryImpl implements StreetCustomRepository {

    @Autowired
    EntityManager en;

    @Override
    public List<Street> findStreetLike(String queryString) {
        List result;
        result = en.createQuery("from Street s where s.locationName like :args")
                .setParameter("args", "%"+queryString+"%")
                .getResultList();
        return result;
    }

    @Override
    public List<Street> findStreetLikeOn(Barangay barangay, String locationName) {
        List result;
        result = en.createQuery("from Street s where s.barangay = :barangay " +
                "and s.locationName like :locationName")
                .setParameter("barangay", barangay)
                .setParameter("locationName", "%"+locationName+"%")
                .getResultList();
        return result;
    }
}
