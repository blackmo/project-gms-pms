package com.vhl.services.account;

import com.vhl.entities.local.Person;
import org.springframework.dao.EmptyResultDataAccessException;

public interface UserAccountCustomRepository {
    public boolean validateUser(String userName, String password) throws EmptyResultDataAccessException;
}
