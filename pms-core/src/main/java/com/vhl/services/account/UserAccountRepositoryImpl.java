package com.vhl.services.account;

import com.vhl.entities.account.UserAccount;
import com.vhl.services.account.UserAccountCustomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

public class UserAccountRepositoryImpl implements UserAccountCustomRepository {
    @Autowired private EntityManager em;

    @Override
    public boolean validateUser(String userName, String password) {
        UserAccount user;
        user = (UserAccount) em.createQuery("from UserAccount u where u.userName=:userName and " +
                "u.password=:password")
                .setParameter("userName", userName)
                .setParameter("password", password)
                .getSingleResult();
        return user != null;
    }
}
