package com.vhl.services.account;

import com.vhl.entities.account.UserAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface UserAccountRepository extends CrudRepository<UserAccount, Integer>, UserAccountCustomRepository {
}
