package com.vhl.interfaces.location;

/**
 * Created by Knight-en on 02/08/2017.
 */
public interface SubLocation<S> extends Place{

    void addSubLocation(S place);

    void removeSubLocation(S place);
}
