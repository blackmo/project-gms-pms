package com.vhl.interfaces.location;


import com.vhl.entities.local.Resident;

/**
 * Created by Knight-en on 19/06/2017.
 */

public interface Place {
    String getLocationName();

    Place getRootLocation();

    Resident addResident(Resident resident);

    int getId();

    int typeOf();
}
