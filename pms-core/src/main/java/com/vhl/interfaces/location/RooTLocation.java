package com.vhl.interfaces.location;

/**
 * Created by Knight-en on 02/08/2017.
 */
public interface RooTLocation <T>extends Place {

    void setRootLocation(T rootLocation);

}
