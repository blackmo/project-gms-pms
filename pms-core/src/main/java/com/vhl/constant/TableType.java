package com.vhl.constant;

/**
 * Created by Knight-en on 01/08/2017.
 */
public interface TableType {
    String PERSON_NAME = "person";
    String RESIDENT_NAME = "resident";
    String MUN_CITY_NAME = "municipality_city";
    String REGION_NAME = "region";
    String STREET_NAME = "street";
    String TOWN_NAME = "town";
    String CLIENT_NAME = "client";
    String CONTRACT_NAME = "contract";
    String ENGINEER_NAME = "engineer";
    String FIXES_NAME = "fixes";
    String MACHINE_NAME = "machine";
    String PMS_NAME = "pms";
    String USERT_ACCOUNT_NAME = "user";

    int PERSON = 0;
    int RESIDENT = 1;
    int MUN_CITY = 2;
    int REGION = 3;
    int STREET = 4;
    int TOWN = 5;
    int CLIENT = 6;
    int CONTRACT = 7;
    int ENGINEER = 8;
    int FIXES = 9;
    int MACHINE = 10;
    int PMS = 11;
    int USER_ACCOUNT = 12;
}
