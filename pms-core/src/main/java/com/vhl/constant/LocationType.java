package com.vhl.constant;

/**
 * Created by Knight-en on 31/07/2017.
 */
public interface LocationType {
    short PROVINCE = 0;
    short MUNICIPALITY_CITY = 1;
    short BARANGAY = 2;
    short STREET = 3;
}
