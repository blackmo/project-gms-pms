package com.vhl.constant;

/**
 * Created by Knight-en on 27/07/2017.
 */
public interface Type {
    String QUIZ = "quiz";
    String ASSIGNMENT = "assignment";
    String EXAM = "exam";
    String PROJECT = "project";
    String RECITATION_PARTICIPATION = "recitation_participation";

    int FEMALE = 0;
    int MALE = 1;
}
