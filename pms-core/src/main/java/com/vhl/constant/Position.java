package com.vhl.constant;

public interface Position {
    String  ENGINEER = "engineer";
    String APP_SPECIALIST = "app-specialist";
    String SUPERVISOR = "supervisor";
    String CSA = "csa";
    String LAB_SPECIALIST = "lab-specialist";
}
