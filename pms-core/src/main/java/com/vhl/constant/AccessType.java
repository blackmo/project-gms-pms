package com.vhl.constant;

/**
 * Created by Knight-en on 05/08/2017.
 */
public interface AccessType {
    int Pending= 0;
    int CLASS_READ = 1;
    int CLASS_READ_ADD = 2;
    int CLASSS_READ_EDIT=3;
    int CLASS_READ_ADD_DELETE=4;
    int CO_ADMIN = 5;
}
