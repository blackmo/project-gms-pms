package com.vhl.constant;

public interface ServiceName {
    String PERON_SERVICE = "/person";
    String RESIDENT_SERVICE = "/resident";
    String REGION_SERVICE = "/region";
    String MUNCITY_SERVICE = "/muncity";
    String BARANGAY_SERVICE = "/barangay";
    String STREET_SERVICE = "/street";
    String CLIENT_SERVICE = "/client";
    String CONTRACT_SERVICE = "/contract";
    String EMPLOYEE_SERVICE = "/employee";
    String FIXES_SERVICE = "/fixes";
    String MACHINE_SERVICE = "/machine";
    String PMS_SERVICE = "/pms";
    String USER_ACCOUNT_SERVICE = "/user";

}
