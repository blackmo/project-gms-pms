package com.vhl.doa.priv;

import com.vhl.entities.priv.Machine;
import com.vhl.services.base.BaseService;
import com.vhl.services.priv.machine.MachineCustomRepository;

public interface MachineDoa extends BaseService<Machine>, MachineCustomRepository {
}
