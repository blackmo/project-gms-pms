package com.vhl.doa.priv;

import com.vhl.entities.priv.Fixes;
import com.vhl.services.base.BaseService;
import com.vhl.services.priv.fixes.FixesCustomRepository;

public interface FixesDoa extends BaseService<Fixes>, FixesCustomRepository {
}
