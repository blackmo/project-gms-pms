package com.vhl.doa.priv;

import com.vhl.entities.priv.Contract;
import com.vhl.services.base.BaseService;
import com.vhl.services.priv.contract.ContractCustomRepository;

public interface ContractDoa extends BaseService<Contract>, ContractCustomRepository {
}
