package com.vhl.doa.priv;

import com.sun.deploy.util.SessionState;
import com.vhl.entities.priv.Client;
import com.vhl.services.base.BaseService;
import com.vhl.services.priv.client.ClientCustomRepository;

import java.util.List;

public interface ClientDoa extends BaseService<Client>, ClientCustomRepository{
    List<Client> getAllClient();
}
