package com.vhl.doa.priv;

import com.vhl.entities.priv.PMS;
import com.vhl.services.base.BaseService;
import com.vhl.services.priv.pms.PMSCustomRepository;

public interface PMSDoa extends BaseService<PMS>, PMSCustomRepository {
}
