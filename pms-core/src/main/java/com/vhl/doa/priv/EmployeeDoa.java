package com.vhl.doa.priv;

import com.vhl.entities.priv.Employee;
import com.vhl.services.base.BaseService;
import com.vhl.services.priv.employee.EmployeeCustomRepository;

public interface EmployeeDoa extends BaseService<Employee>, EmployeeCustomRepository {
}
