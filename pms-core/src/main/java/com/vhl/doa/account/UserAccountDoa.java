package com.vhl.doa.account;

import com.vhl.entities.account.UserAccount;
import com.vhl.services.account.UserAccountCustomRepository;
import com.vhl.services.base.BaseService;

public interface UserAccountDoa extends BaseService<UserAccount>, UserAccountCustomRepository {
}
