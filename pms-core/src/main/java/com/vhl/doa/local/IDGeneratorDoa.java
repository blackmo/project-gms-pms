package com.vhl.doa.local;

import com.vhl.entities.local.CustomTableIDGenerator;
import com.vhl.services.base.BaseService;

public interface IDGeneratorDoa extends BaseService<CustomTableIDGenerator>{
    int generateIdOn(int tableType);
}
