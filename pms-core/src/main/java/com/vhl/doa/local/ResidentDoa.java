package com.vhl.doa.local;

import com.vhl.entities.local.Resident;
import com.vhl.services.base.BaseService;
import com.vhl.services.local.residence.ResidenceCustomService;

public interface ResidentDoa extends BaseService<Resident>, ResidenceCustomService {
}
