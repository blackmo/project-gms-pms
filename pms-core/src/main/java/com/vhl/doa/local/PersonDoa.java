package com.vhl.doa.local;

import com.vhl.entities.local.Person;
import com.vhl.services.base.BaseService;
import com.vhl.services.local.person.PersonCustomRepository;

public interface PersonDoa extends BaseService<Person>,PersonCustomRepository {
}
