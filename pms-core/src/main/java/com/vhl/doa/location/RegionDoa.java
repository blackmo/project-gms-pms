package com.vhl.doa.location;

import com.vhl.entities.location.Region;
import com.vhl.services.base.BaseService;
import com.vhl.services.location.region.RegionCustomRepository;

public interface RegionDoa extends BaseService<Region>,RegionCustomRepository {

}
