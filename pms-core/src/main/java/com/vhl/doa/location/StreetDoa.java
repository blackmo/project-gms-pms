package com.vhl.doa.location;

import com.vhl.entities.location.Street;
import com.vhl.services.base.BaseService;
import com.vhl.services.location.street.StreetCustomRepository;

public interface StreetDoa extends BaseService<Street>, StreetCustomRepository {
}
