package com.vhl.doa.location;

import com.vhl.entities.location.Barangay;
import com.vhl.services.base.BaseService;
import com.vhl.services.location.barangay.BarangayCustomRepository;

public interface BarangayDoa extends BaseService<Barangay>, BarangayCustomRepository {
}
