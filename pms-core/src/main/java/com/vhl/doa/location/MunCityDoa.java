package com.vhl.doa.location;

import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.services.base.BaseService;
import com.vhl.services.location.mun_city.MunCityCustomRepository;

public interface MunCityDoa extends BaseService<Municipality_CityTown>, MunCityCustomRepository {
}
