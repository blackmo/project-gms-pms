package com.vhl.entities.location;


import com.vhl.constant.LocationType;
import com.vhl.constant.TableType;
import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;
import com.vhl.interfaces.location.RooTLocation;
import com.vhl.interfaces.location.SubLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Knight-en on 19/06/2017.
 */
@Entity
public class Barangay implements SubLocation<Street>, RooTLocation<Municipality_CityTown>,Serializable{

    private int id;
    private String locationName;
    private List<Street> listStreet;
    private Municipality_CityTown muncity;
    private List<Resident> listResident;
    private boolean city = false;
    @Transient
    private final short locationType = LocationType.BARANGAY;

    //-- getters
    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getLocationName() {
        return locationName;
    }

    @OneToMany(mappedBy = "barangay")
    public List<Street> getListStreet() {
        return listStreet;
    }

    @ManyToOne
    public Municipality_CityTown getMuncity() {
        return muncity;
    }

    @OneToMany(mappedBy = "barangay")
    public List<Resident> getListResident() {
        return listResident;
    }

    //-- setters
    public void setId(int id) {
        this.id = id;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setListStreet(List<Street> listStreet) {
        this.listStreet = listStreet;
    }

    public void setListResident(List<Resident> listResident) {
        this.listResident = listResident;
    }

    public void setMuncity(Municipality_CityTown muncity) {
        this.muncity = muncity;
    }

    //-- custom public methods
    @Override
    @Transient
    public Place getRootLocation() {
        return muncity;
    }

    @Override
    public void addSubLocation(Street place) {
        listStreet.add(place);
    }

    @Override
    public void removeSubLocation(Street place) {
        listStreet.remove(place);
    }

    @Override
    public void setRootLocation(Municipality_CityTown rootLocation) {
        muncity = rootLocation;
    }

    @Override
    public Resident addResident(Resident resident) {
        listResident.add(resident);
        return resident;
    }

    @Override
    public int typeOf() {
        return TableType.TOWN;
    }
}
