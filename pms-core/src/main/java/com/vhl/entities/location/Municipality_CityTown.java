package com.vhl.entities.location;



import com.vhl.constant.LocationType;
import com.vhl.constant.TableType;
import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;
import com.vhl.interfaces.location.RooTLocation;
import com.vhl.interfaces.location.SubLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Knight-en on 01/08/2017.
 */
@Entity(name = "municipality_city")
public class Municipality_CityTown implements SubLocation<Barangay>, RooTLocation<Region>, Serializable {
    private int id;
    private String locationName;
    private List<Barangay> listBarangay;
    private Region region;
    private List<Resident> listResident;
    private boolean city;

    @Transient
    private final int locationType = LocationType.MUNICIPALITY_CITY;

    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getLocationName() {
        return locationName;
    }

    @OneToMany(mappedBy = "municipality_city")
    public List<Barangay> getListBarangay() {
        return listBarangay;
    }

    @ManyToOne
    public Region getRegion() {
        return region;
    }

    @OneToMany(mappedBy = "municipality_city")
    public List<Resident> getListResident() {
        return listResident;
    }

    @Column
    public boolean isCity() {
        return city;
    }

    //-- SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setListBarangay(List<Barangay> listBarangay) {
        this.listBarangay = listBarangay;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setListResident(List<Resident> listResident) {
        this.listResident = listResident;
    }

    public void setCity(boolean city) {
        this.city = city;
    }

    //-- CUSTOM PUBLIC METHODS

    @Override
    @Transient
    public Place getRootLocation() {
        return region;
    }

    @Override
    @Transient
    public void addSubLocation(Barangay barangay) {
        listBarangay.add(barangay);
    }

    @Override
    public void removeSubLocation(Barangay barangay) {
        listBarangay.remove(barangay);
    }

    @Override
    public Resident addResident(Resident resident) {
        return null;
    }

    @Override
    public int typeOf() {
        return TableType.MUN_CITY;
    }

    @Override
    public void setRootLocation(Region rootLocation) {
        region = rootLocation;
    }
}
