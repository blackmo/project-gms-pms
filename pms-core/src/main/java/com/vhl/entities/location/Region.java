package com.vhl.entities.location;


import com.vhl.constant.LocationType;
import com.vhl.constant.TableType;
import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;
import com.vhl.interfaces.location.SubLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Knight-en on 19/06/2017.
 */
@Entity
public class Region implements SubLocation<Municipality_CityTown>, Serializable {
    private int id;
    private String locationName;
    private List<Municipality_CityTown> municipality_city;
    private List<Resident> listResident;
    @Transient
    private final short locationType = LocationType.PROVINCE;

    //-- GETTER
    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getLocationName() {
        return locationName;
    }


    @OneToMany(mappedBy = "region")
    public List<Resident> getListResident() {
        return listResident;
    }

    @OneToMany(mappedBy = "region")
    public List<Municipality_CityTown> getMunicipality_city() {
        return municipality_city;
    }

    //-- SETTER
    public void setId(int id) {
        this.id = id;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }


    public void setListResident(List<Resident> listResident) {
        this.listResident = listResident;
    }

    public void setMunicipality_city(List<Municipality_CityTown> municipality_city) {
        this.municipality_city = municipality_city;
    }

    //-- custom public methods
    @Override
    @Transient
    public Place getRootLocation() {
        return null;
    }

    @Override
    public void addSubLocation(Municipality_CityTown place) {
        municipality_city.add(place);
        System.out.println("sub region added;");
    }

    @Override
    public void removeSubLocation(Municipality_CityTown place) {
        municipality_city.remove(place);
    }

    @Override
    public Resident addResident(Resident resident) {
        listResident.add(resident);
        return resident;
    }

    @Override
    public int typeOf() {
        return TableType.REGION;
    }


}
