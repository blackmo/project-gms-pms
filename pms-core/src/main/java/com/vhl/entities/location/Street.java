package com.vhl.entities.location;



import com.vhl.constant.LocationType;
import com.vhl.constant.TableType;
import com.vhl.entities.local.Resident;
import com.vhl.interfaces.location.Place;
import com.vhl.interfaces.location.RooTLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Knight-en on 19/06/2017.
 */
@Entity
public class Street  implements RooTLocation<Barangay>, Serializable {
    private int id;
    private String locationName;
    private Barangay barangay;
    private List<Resident> listResident;
    @Transient
    private final short locationType = LocationType.STREET;

    //-- getters
    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getLocationName() {
        return locationName;
    }

    @ManyToOne
    public Barangay getBarangay() {
        return barangay;
    }

    @OneToMany(mappedBy = "street")
    public List<Resident> getListResident() {
        return listResident;
    }

    //-- setters
    public void setId(int id) {
        this.id = id;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setBarangay(Barangay barangay) {
        this.barangay = barangay;
    }

    public void setListResident(List<Resident> listResident) {
        this.listResident = listResident;
    }

    //-- custom public methods

    @Override
    @Transient
    public Place getRootLocation() {
        return barangay;
    }


    @Override
    public Resident addResident(Resident resident) {
        listResident.add(resident);
        return resident;
    }

    @Override
    public int typeOf() {
        return TableType.STREET;
    }

    @Override
    public void setRootLocation(Barangay rootLocation) {
        barangay =rootLocation;
    }
}
