package com.vhl.entities.account;

import com.vhl.entities.local.Person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
public class UserAccount implements Serializable{
    private int id;
    private String userName;
    private String password;
    private String hint;
    private int accessLevel;
    private Person person;

    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @Column(length = 15)
    public String getUserName() {
        return userName;
    }

    @Column(length = 16)
    public String getPassword() {
        return password;
    }

    @Column(length = 20)
    public String getHint() {
        return hint;
    }

    @Column
    public int getAccessLevel() {
        return accessLevel;
    }

    @OneToOne
    public Person getPerson() {
        return person;
    }

    //-- SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
