package com.vhl.entities.local;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Knight-en on 14/06/2017.
 */
@Entity
public class Person implements Serializable {
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date birthday;
    private String contactNo;
    private List<Resident> listResidents;
    private List<Person> contactPerson;
    private short gender;
    private String fullName;


    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getContactNo() {
        return contactNo;
    }

    @Column
    public String getFirstName() {
        return firstName;
    }

    @Column
    public String getMiddleName() {
        return middleName;
    }

    @Column
    public String getLastName() {
        return lastName;
    }

    @Column
    @Temporal(TemporalType.DATE)
    public Date getBirthday() {
        return birthday;
    }

    @ManyToMany
    @JoinTable(name = "person_contact", joinColumns = @JoinColumn(name = "person"), inverseJoinColumns =
    @JoinColumn(name = "contact"))
    public List<Person> getContactPerson() {
        return contactPerson;
    }

    @Column
    public short getGender() {
        return gender;
    }

    @OneToMany(mappedBy = "person")
    public List<Resident> getListResidents() {
        return listResidents;
    }

    @Transient
    public String getFullName() {
        return String.format("%s, %s %s", lastName, firstName, middleName);
    }

    //-- SETTERS

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public void setContactPerson(List<Person> contactPerson) {
        this.contactPerson = contactPerson;
    }

    public void setGender(short gender) {
        this.gender = gender;
    }

    public void setListResidents(List<Resident> listResidents) {
        this.listResidents = listResidents;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    //-- PUBLIC CUSTOM METHODS
    public String toString() {
        return String.format("%s, %s %s", lastName, firstName, middleName);
    }

    public void addContactPerson(Person person) {
        contactPerson.add(person);
    }
}
