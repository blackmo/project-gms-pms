package com.vhl.entities.local;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Knight-en on 01/08/2017.
 */
@Entity(name = "deleted_ids")
public class DeletedIds implements Serializable {

    public DeletedIds(int id) {
        this.id = id;
    }
    public DeletedIds(){}

    @Id
    private int id;
    @ManyToMany
    @JoinTable(name = "deleted_entries",joinColumns = @JoinColumn(name = "deleted_id"), inverseJoinColumns =
    @JoinColumn(name = "from_id"))
    private List<CustomTableIDGenerator> from_id;

    public int getId() {
        return id;
    }

    public List<CustomTableIDGenerator> getFrom_id() {
        return from_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFrom_id(List<CustomTableIDGenerator> from_id) {
        this.from_id = from_id;
    }

    public void addDeletedFrom(CustomTableIDGenerator customTableIDGenerator) {
        if (from_id==null) from_id = new ArrayList<>();
        from_id.add(customTableIDGenerator);
    }
}
