package com.vhl.entities.local;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Knight-en on 01/08/2017.
 */
@Entity(name = "table_ids")
public class CustomTableIDGenerator implements Serializable {
    public CustomTableIDGenerator(int table_id) {
        this.id = table_id;
    }

    public CustomTableIDGenerator() {

    }

    private int id;
    private String table_name;
    private int nextValue = 1;
    private List<DeletedIds> listDeletedIds;
    @Transient
    int holder;
    public static CustomTableIDGenerator instance;

    //-- GETTER
    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getTable_name() {
        return table_name;
    }

    @Column
    public int getNextValue() {
        return nextValue;
    }

    @ManyToMany(mappedBy = "from_id")
    public List<DeletedIds> getListDeletedIds() {
        return listDeletedIds;
    }

    @Transient
    public static CustomTableIDGenerator getInstance() {
        if (instance == null) {
            instance = new CustomTableIDGenerator();
        }
        return instance;
    }

    //-- SETTER
    public void setId(int id) {
        this.id = id;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public void setNextValue(int nextValue) {
        this.nextValue = nextValue;
    }

    public void setListDeletedIds(List<DeletedIds> listDeletedIds) {
        this.listDeletedIds = listDeletedIds;
    }

    //-- CUSTOM PUBLIC METHODS
    public int generateID() {
        holder = nextValue;
        nextValue++;
        return holder;
    }

    //-- CUSTOM PRIVATE METHODS
    public void setDeleteId(DeletedIds id) {
        listDeletedIds.add(id);
    }

    public boolean hasdeleted() {
        if (listDeletedIds.size() == 0) return false;
        else return true;
    }
}
