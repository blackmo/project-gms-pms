package com.vhl.entities.local;

import com.vhl.entities.location.Barangay;
import com.vhl.entities.location.Municipality_CityTown;
import com.vhl.entities.location.Region;
import com.vhl.entities.location.Street;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Knight-en on 19/06/2017.
 */
@Entity
public class Resident implements Serializable {
    private int id;
    private Street street;
    private Barangay barangay;
    private Municipality_CityTown municipality_city;
    private Region region;
    private Person person;

    //-- getters

    @Id
    public int getId() {
        return id;
    }

    @ManyToOne
    public Street getStreet() {
        return street;
    }

    @ManyToOne
    public Barangay getBarangay() {
        return barangay;
    }

    @ManyToOne
    public Municipality_CityTown getMunicipality_city() {
        return municipality_city;
    }

    @ManyToOne
    public Region getRegion() {
        return region;
    }

    @ManyToOne()
    public Person getPerson() {
        return person;
    }

    //setters
    public void setId(int id) {
        this.id = id;
    }

    public void setStreet(Street street) {
        this.street = street;
    }

    public void setBarangay(Barangay barangay) {
        this.barangay = barangay;
    }

    public void setMunicipality_city(Municipality_CityTown municipality_city) {
        this.municipality_city = municipality_city;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    //custom public methods
    public String toString() {
        return String.format("%s, %s, %s, %s", street, barangay, municipality_city, region);
    }
}
