package com.vhl.entities.priv;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class Machine implements Serializable{
    private int id;
    private String name;
    private String description;
    private String serial;
    private Date warranty;
    private String warrantyStatus;
    private String machineStatus;
    private Client owner;
    private List<Fixes> listFixes;
    private List<PMS> listPMS;


    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @Column(length = 50, nullable = false)
    public String getName() {
        return name;
    }

    @Column
    public String getDescription() {
        return description;
    }

    @Column(length = 50)
    public String getSerial() {
        return serial;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getWarranty() {
        return warranty;
    }

    @Column(length = 20)
    public String getWarrantyStatus() {
        return warrantyStatus;
    }

    @Column(length = 30)
    public String getMachineStatus() {
        return machineStatus;
    }

    @ManyToOne
    public Client getOwner() {
        return owner;
    }

    @OneToMany(mappedBy = "machine")
    public List<Fixes> getListFixes() {
        return listFixes;
    }

    @OneToMany(mappedBy = "machine")
    public List<PMS> getListPMS() {
        return listPMS;
    }

    //-- SETTERS

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setWarranty(Date warranty) {
        this.warranty = warranty;
    }

    public void setWarrantyStatus(String warrantyStatus) {
        this.warrantyStatus = warrantyStatus;
    }

    public void setMachineStatus(String machineStatus) {
        this.machineStatus = machineStatus;
    }

    public void setOwner(Client owner) {
        this.owner = owner;
    }

    public void setListFixes(List<Fixes> listFixes) {
        this.listFixes = listFixes;
    }

    public void setListPMS(List<PMS> listPMS) {
        this.listPMS = listPMS;
    }

    //-- CUSTOM METHODS
    public void addFixes(Fixes fixes) {
        listFixes.add(fixes);
    }

    public void addPMS(PMS pms) {
        listPMS.add(pms);
    }
}
