package com.vhl.entities.priv;

import com.vhl.constant.Position;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class Fixes implements Serializable {
    private int id;
    private String reportDescription;
    private String reportedBy;
    private String status;
    private Date reportedDate;
    private Date dateFixed;
    private List<Employee> stationedEngineers;
    private List<String> logs;
    private Machine machine;

    //-- GETTERS

    @Id
    public int getId() {
        return id;
    }

    @Column
    public String getReportDescription() {
        return reportDescription;
    }

    @Column
    public String getReportedBy() {
        return reportedBy;
    }

    @Column
    public String getStatus() {
        return status;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getReportedDate() {
        return reportedDate;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getDateFixed() {
        return dateFixed;
    }

    @OneToMany
    public List<Employee> getStationedEngineers() {
        return stationedEngineers;
    }

    @ElementCollection
    @CollectionTable(name = "fixes_logs", joinColumns = @JoinColumn(name = "fix_id"))
    @Column
        public List<String> getLogs() {
        return logs;
    }

    @ManyToOne
    public Machine getMachine() {
        return machine;
    }

    //-- SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }

    public void setReportedBy(String reportedBy) {
        this.reportedBy = reportedBy;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setReportedDate(Date reportedDate) {
        this.reportedDate = reportedDate;
    }

    public void setDateFixed(Date dateFixed) {
        this.dateFixed = dateFixed;
    }

    public void setStationedEngineers(List<Employee> stationedEngineers) {
        this.stationedEngineers = stationedEngineers;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    //-- CUSTOM METHODS
    public void addLog(String log) {
        this.logs.add(log);
    }

    public void addStationedEngineer(Employee employee) {
        if (employee.getPosition().equals(Position.ENGINEER))
        this.stationedEngineers.add(employee);
    }

}
