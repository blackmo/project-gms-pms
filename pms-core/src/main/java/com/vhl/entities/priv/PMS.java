package com.vhl.entities.priv;

import com.vhl.constant.Position;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class PMS implements Serializable {
    private  int id;
    private  Machine machine;
    private Employee assignedEngineer;
    private Date lastPMS;
    private Date nextPMS;
    private String remarks;

    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @ManyToOne
    public Machine getMachine() {
        return machine;
    }

    @ManyToOne
    public Employee getAssignedEngineer() {
        return assignedEngineer;
    }

    @Temporal(TemporalType.DATE)
    public Date getLastPMS() {
        return lastPMS;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getNextPMS() {
        return nextPMS;
    }

    @Column(length = 30, nullable = false)
    public String getRemarks() {
        return remarks;
    }

    //-- SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }

    public void setAssignedEngineer(Employee assignedEmployee) {
        if (assignedEmployee.getPosition().equals(Position.ENGINEER))
        this.assignedEngineer = assignedEmployee;
    }

    public void setLastPMS(Date lastPMS) {
        this.lastPMS = lastPMS;
    }

    public void setNextPMS(Date nextPMS) {
        this.nextPMS = nextPMS;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
