package com.vhl.entities.priv;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
public class Contract implements Serializable{
    private int id;
    private String contractName;
    private Client client;
    private List<Machine> machineList;
    private Date contractDate;
    private Date contractEndDate;
    private String contractStatus;
    private String note;

    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @Column(length = 50, nullable = false)
    public String getContractName() {
        return contractName;
    }

    @OneToOne
    public Client getClient() {
        return client;
    }

    @OneToMany
    public List<Machine> getMachineList() {
        return machineList;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getContractDate() {
        return contractDate;
    }

    @Temporal(value = TemporalType.DATE)
    public Date getContractEndDate() {
        return contractEndDate;
    }

    @Column(length = 20)
    public String getContractStatus() {
        return contractStatus;
    }

    @Column
    public String getNote() {
        return note;
    }

    //-- SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setMachineList(List<Machine> machineList) {
        this.machineList = machineList;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public void setContractEndDate(Date contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public void setNote(String note) {
        this.note = note;
    }

    //-- CUSTOM METHODS
    public void addMachineToContract(Machine machine) {
        machineList.add(machine);
    }
}
