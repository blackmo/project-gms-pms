package com.vhl.entities.priv;

import com.vhl.entities.local.Person;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Employee implements Serializable{
    private int id;
    private Person person;
    private List<PMS> listAssignments;
    private int currentAssignment_PMSId;
    private String assignmentStatus;
    private String position;

    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @OneToOne
    public Person getPerson() {
        return person;
    }

    @OneToMany(mappedBy = "assignedEngineer")
    public List<PMS> getListAssignments() {
        return listAssignments;
    }


    @Column
    public int getCurrentAssignment_PMSId() {
        return currentAssignment_PMSId;
    }

    @Column
    public String getAssignmentStatus() {
        return assignmentStatus;
    }

    @Column(length = 20)
    public String getPosition() {
        return position;
    }

    //-- SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public void setListAssignments(List<PMS> listAssignments) {
        this.listAssignments = listAssignments;
    }

    public void setCurrentAssignment_PMSId(int currentAssignment_PMSId) {
        this.currentAssignment_PMSId = currentAssignment_PMSId;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setAssignmentStatus(String assignmentStatus) {
        this.assignmentStatus = assignmentStatus;
    }

    //-- CUSTOM METHODS
    public void addAssignent(PMS pms) {
        listAssignments.add(pms);
    }
}
