package com.vhl.entities.priv;

import com.vhl.entities.local.Person;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Client implements Serializable {
    private int id;
    private String clientName;
    private String contactNumber;
    private List<Person> listContactPerson;


    //-- GETTERS
    @Id
    public int getId() {
        return id;
    }

    @Column(nullable = false, length = 50)
    public String getClientName() {
        return clientName;
    }

    @Column(nullable = false, length = 15)
    public String getContactNumber() {
        return contactNumber;
    }

    @OneToMany
    public List<Person> getListContactPerson() {
        return listContactPerson;
    }

    public void setId(int id) {
        this.id = id;
    }


    //-- SETTERS
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setListContactPerson(List<Person> listContactPerson) {
        this.listContactPerson = listContactPerson;
    }

    //-- CUSTOM METHODS
    public void addContactPerson(Person person) {
        listContactPerson.add(person);
    }

}