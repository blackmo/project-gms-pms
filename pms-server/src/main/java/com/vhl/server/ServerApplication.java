package com.vhl.server;

import com.vhl.doa.local.IDGeneratorDoa;
import com.vhl.generator.IDTableInitialization;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ServerApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ServerApplication.class, args);
        IDTableInitialization x = new IDTableInitialization();
        x.setGeneratorDoa(context.getBean(IDGeneratorDoa.class));
        x.checkTableInitialization();
    }
}
