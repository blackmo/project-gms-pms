package com.vhl.config;

import com.vhl.constant.ServiceName;
import com.vhl.doa.account.UserAccountDoa;
import com.vhl.doa.local.PersonDoa;
import com.vhl.doa.local.ResidentDoa;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

@Configuration
public class ClientConfiguration {
    @Value("${server.url}")
    private String serverUrl;

    @Bean
    public HttpInvokerProxyFactoryBean personInvoker() {
        System.out.println("form client");
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ ServiceName.PERON_SERVICE);
        invoker.setServiceInterface(PersonDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean residentInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.RESIDENT_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean regionInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.REGION_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean muncityInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.MUNCITY_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean townInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.BARANGAY_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean streetInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.STREET_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean clientInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.CLIENT_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean contractInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.CONTRACT_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean engineerInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.BARANGAY_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean fixesInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.FIXES_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean macineInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.MACHINE_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean pmsInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.PMS_SERVICE);
        invoker.setServiceInterface(ResidentDoa.class);
        return invoker;
    }

    @Bean
    public HttpInvokerProxyFactoryBean userInvoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl(serverUrl+ServiceName.USER_ACCOUNT_SERVICE);
        invoker.setServiceInterface(UserAccountDoa.class);
        return invoker;
    }

}
