package com.vhl;

import com.vhl.doa.account.UserAccountDoa;
import com.vhl.doa.local.PersonDoa;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
//
//@SpringBootApplication(exclude = {EmbeddedServletContainerAutoConfiguration.class,
//        WebMvcAutoConfiguration.class})
//@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class ClientApplication {
//    public static void main(String[] args) {
//        ConfigurableApplicationContext context = SpringApplication.run(ClientApplication.class, args);
//        PersonDoa personDoa = context.getBean(PersonDoa.class);
//        UserAccountDoa accountDoa = context.getBean(UserAccountDoa.class);
//        System.out.println(accountDoa.validateUser("user", "password"));
//    }
}
